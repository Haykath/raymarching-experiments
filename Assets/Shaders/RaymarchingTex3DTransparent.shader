// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'


Shader "Raymarching/RaymarchingTex3DTrans" {
    Properties {
        _StepMax ("StepMax", Float ) = 200
		_Volume ("Volume Texture", 3D) = "" {}

		[HideInInspector] _CuttingPlanePos ("Cutting Plane Position", Vector) = (0,0,0,1)
		[HideInInspector] _CuttingPlaneNormal ("Cutting Plane Normal", Vector) = (0,1,0,0)

		[HideInInspector] _BoundingBoxMin ("Bounding Box Minimum", Vector) = (-1,-1,-1,1)
		[HideInInspector] _BoundingBoxMax ("Bounding Box Maximum", Vector) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha 
                       
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
			#include "Raymarching.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            
            uniform float _StepMax;

			uniform float4 _CuttingPlanePos;
			uniform float4 _CuttingPlaneNormal;

			uniform float4 _BoundingBoxMin;
			uniform float4 _BoundingBoxMax;

			uniform sampler3D _Volume;


            struct VertexInput {
                float4 vertex : POSITION;
            }
;
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };

            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;

                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }

            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);

                Ray eyeRay;
				eyeRay.origin = _WorldSpaceCameraPos;
				eyeRay.direction = (-1*viewDirection);

				Bounds bounds;
				bounds.Min = _BoundingBoxMin;
				bounds.Max = _BoundingBoxMax;

				Plane cutPlane;
				cutPlane.pos = _CuttingPlanePos.xyz;
				cutPlane.norm = _CuttingPlaneNormal.xyz;

				float4 raymarchRGBA = RayMarchPS(eyeRay, _StepMax, bounds, cutPlane, _Volume);

                ////// Lighting:

				float4 finalRGBA = raymarchRGBA;

                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }

            ENDCG
        }
       
    }
    FallBack "Diffuse"

}
