﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Generate3DFromImg : EditorWindow
{
    const string Title = "3D Texture Generator";
    const string fileExt = ".asset";

    Vector2 windowScroll;

    private string path = "Assets/Tex3D/";
    private string assetName = "TextureName";

    private Texture2D[] slices = new Texture2D[8];
    private int sliceCount = 8;
    private bool sliceExpand = false;

    private Vector3 dimensions = new Vector3(8,8,8);
    private bool increaseVisibility = false;

    [MenuItem("Texture3D/TextureGenerator")]
    public static void ShowWindow()
    {
        var window = GetWindow(typeof(Generate3DFromImg));
        window.Show();
    }

    void OnEnable()
    {
        titleContent = new GUIContent(Title);
    }

    // Use this for initialization
    void OnGUI()
    {
        GUILayout.BeginVertical();
        EditorGUILayout.BeginVertical();
        windowScroll = EditorGUILayout.BeginScrollView(windowScroll);

        GUILayout.Label("Texture Parameters:");
        EditorGUILayout.Separator();

        //Texture params
        dimensions = EditorGUILayout.Vector3Field("Texture Dimensions", dimensions);
        increaseVisibility = EditorGUILayout.Toggle("Increase Visibility",increaseVisibility);
        EditorGUILayout.Separator();
        DrawArrayEditor();
        EditorGUILayout.Separator();

        //File
        GUILayout.Label("Asset File:");
        EditorGUILayout.Separator();

        path = EditorGUILayout.TextField("Path", path);
        assetName = EditorGUILayout.TextField("File Name", assetName);
        EditorGUILayout.Separator();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button("Save Asset", GUILayout.ExpandWidth(false)))
        {
            Texture3D tex = GenerateTexture();
            GenerateAsset(tex);
            AssetDatabase.Refresh();
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
        GUILayout.EndVertical();
    }

    Texture3D GenerateTexture()
    {
        // sort
        System.Array.Sort(slices, (x, y) => x.name.CompareTo(y.name));

        // use a bunch of memory!
        Texture3D tex = new Texture3D((int)dimensions.x, (int)dimensions.y, (int)dimensions.z, TextureFormat.ARGB32, false);

        var w = tex.width;
        var h = tex.height;
        var d = tex.depth;

        // skip some slices if we can't fit it all in
        var countOffset = (slices.Length - 1) / (float)d;

        var volumeColors = new Color[w * h * d];

        var sliceCount = 0;
        var sliceCountFloat = 0f;
        for (int z = 0; z < d; z++)
        {
            sliceCountFloat += countOffset;
            sliceCount = Mathf.FloorToInt(sliceCountFloat);
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var idx = x + (y * w) + (z * (w * h));
                    volumeColors[idx] = slices[sliceCount].GetPixelBilinear(x / (float)w, y / (float)h);
                    if (increaseVisibility)
                    {
                        volumeColors[idx].a *= volumeColors[idx].r;
                    }
                }
            }
        }

        tex.SetPixels(volumeColors);
        tex.Apply();
        return tex;
    }

    void GenerateAsset(Texture3D tex)
    {
        AssetDatabase.CreateAsset(tex, path + assetName + fileExt);
    }

    // Use this for initialization
    void DrawArrayEditor()
    {
        sliceExpand = EditorGUILayout.Foldout(sliceExpand, "Slices");
        if (sliceExpand)
        {
            int x = 0;
            sliceCount = EditorGUILayout.IntField("Size", sliceCount);
            if (slices.Length != sliceCount)
            {
                Texture2D[] newArray = new Texture2D[sliceCount];
                for (x = 0; x < sliceCount; x++) {
                    if (slices.Length > x) {
                        newArray[x] = slices[x];
                    }
                }
                slices = newArray;
            }
            for (x = 0; x < slices.Length; x++) {
                slices[x] = EditorGUILayout.ObjectField("Element " + x, slices[x], typeof(Texture2D), false) as Texture2D;
            }
        }
    }
}

