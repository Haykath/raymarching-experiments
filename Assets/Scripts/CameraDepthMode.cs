﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class CameraDepthMode : MonoBehaviour {

    Camera c;
    public DepthTextureMode depthMode = DepthTextureMode.Depth;

	// Use this for initialization
	void Awake () {
        c = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        c.depthTextureMode = depthMode;
    }
}
