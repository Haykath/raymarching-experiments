﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RaymarchingCuttingPlane : MonoBehaviour {

    const string CutPlanePosProperty = "_CuttingPlanePos";
    const string CutPlaneNormProperty = "_CuttingPlaneNormal";

    public Transform cuttingPlane;
    private Renderer r;

    Vector4 planePos = new Vector4(0, 0, 0, 1);
    Vector4 planeNorm = new Vector4(0, 1, 0, 0);
	
    void Start()
    {
        r = GetComponent<Renderer>();
    }

	// Update is called once per frame
	void Update () {
        UpdatePosNorm();
        UpdateShader();
	}

    void UpdatePosNorm()
    {
        if (!cuttingPlane)
            return;

        planePos = cuttingPlane.position;
        planeNorm = cuttingPlane.up;
    }

    void UpdateShader()
    {
        if (!r)
            return;

        if (Application.isPlaying)
        {
            r.material.SetVector(CutPlanePosProperty, planePos);
            r.material.SetVector(CutPlaneNormProperty, planeNorm);
        }
        else
        {
            r.sharedMaterial.SetVector(CutPlanePosProperty, planePos);
            r.sharedMaterial.SetVector(CutPlaneNormProperty, planeNorm);
        }
    }
}
